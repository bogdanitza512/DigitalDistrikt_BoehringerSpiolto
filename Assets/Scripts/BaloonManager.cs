﻿using System;
using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class BaloonManager : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    #region Nested Types

    public enum InteractionPhase { NoInteraction, BeginDrag, Dragging, EndDrag }

    #endregion

    #region Fields and Properties

    public GameController gameCtrl;

    public Image baloonImage;

    public Image greyBaloons;

    public Transform offScreen;

    public TMP_Text titleText;

    public InteractionPhase interactionPhase;

    public bool isInFinalPosition = false;

    public float thresholdMultiplier = 1.0f;
    public Vector3 initalPos;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _BaloonImageChildName = "Image_Baloon",
                                   string _TitleTextChildName = "Text_Title")
    {
        initalPos = transform.position;

        foreach(Transform child in transform)
        {
            if(child.name == _BaloonImageChildName)
            {
                baloonImage = child.GetComponent<Image>();
            }

            if (child.name == _TitleTextChildName)
            {
                titleText = child.GetComponent<TMP_Text>();
            }
        }

    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.NoInteraction &&
           isInFinalPosition == false)
        {
            interactionPhase = InteractionPhase.BeginDrag;
            //transform.SetSiblingIndex(puzzleManager.pieceTargetDict.Count - 1);
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.BeginDrag)
            interactionPhase = InteractionPhase.Dragging;
        if (interactionPhase == InteractionPhase.Dragging)
        {
            var mouseScreenPos =
                Input.mousePosition.WithZ(gameCtrl.masterCanvas.planeDistance);
            var mouseWorldPos =
                gameCtrl.masterCanvas.worldCamera.ScreenToWorldPoint(mouseScreenPos);

            transform.position = mouseWorldPos;

            if (CheckIfNearTarget(thresholdMultiplier *
                                  gameCtrl.thresholdTargetDistance))
            {
                LockToTargetPos();
            }
        }
    }

    public void MoveOffScreen()
    {
        transform.DOMove(offScreen.position, gameCtrl.tweenDuration);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.Dragging)
        {
            interactionPhase = InteractionPhase.EndDrag;

            if (CheckIfNearTarget(thresholdMultiplier *
                                  gameCtrl.thresholdTargetDistance))
            {
                LockToTargetPos();
            }
            else
            {
                transform.DOMove(initalPos, gameCtrl.tweenDuration);
            }

            interactionPhase = InteractionPhase.NoInteraction;
        }
    }

    bool CheckIfNearTarget(float threshold)
    {
        return Vector3.Distance(transform.position, gameCtrl.target.position) < threshold;

    }

    private void LockToTargetPos()
    {
        transform.DOMove(gameCtrl.target.position, gameCtrl.tweenDuration / 2)
                 .OnComplete(() => StartInflating());
        isInFinalPosition = true;
        interactionPhase = InteractionPhase.NoInteraction;
        gameCtrl.Notify_BaloonLockedToTarget();
    }

    private void StartInflating()
    {
        DOTween.Sequence()
               .Append(baloonImage.transform.DOScale(1, gameCtrl.tweenDuration))
               .Join(titleText.DOFade(1, gameCtrl.tweenDuration * 2))
               .OnComplete(() => gameCtrl.Notify_BaloonInflated(this));
    }

    public void FadeOutGreyBaloons()
    {
        greyBaloons.DOFade(0, gameCtrl.tweenDuration);
    }

    #endregion

}

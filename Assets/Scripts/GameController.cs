﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ScreenMgr;
using System;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class GameController : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public Canvas masterCanvas;
    public ScreenManager masterScreenManager;
    public BaseScreen endHubScreen;
    public Transform target;
    public Transform offScreen;
    public Dictionary<BaloonManager, BaseScreen> baloonToInfoScreenDict = 
        new Dictionary<BaloonManager, BaseScreen>();
    public ScreenManager infoScreenManager;
    public Image sprayBubblesImage;
    public float thresholdTargetDistance;
    public float tweenDuration;
    public int score;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }


    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _TargetChildName = "[Baloon_Target]",
                                   string _OffScreenChildName = "[Baloon_OffScreen]",
                                   string _SprayBubblesImageChildName = "Image_Bubbles")
    {
        foreach(Transform child in transform)
        {
            var baloon = child.GetComponent<BaloonManager>();
            if(baloon != null)
            {
                baloonToInfoScreenDict.Add(baloon, null);
                baloon.AutoPopulateFields();
                baloon.gameCtrl = this;
            }

            if(child.name == _TargetChildName)
            {
                target = child;
            }

            if (child.name == _OffScreenChildName)
            {
                offScreen = child;
            }

            if (child.name == _SprayBubblesImageChildName)
            {
                sprayBubblesImage = child.GetComponent<Image>();
            }
        }
    }

    public void Notify_BaloonLockedToTarget()
    {
        sprayBubblesImage.DOFade(1, tweenDuration / 2);
    }

    public void Notify_BaloonInflated(BaloonManager baloon)
    {
        score++;
        infoScreenManager.ShowScreen(baloonToInfoScreenDict[baloon]);
        sprayBubblesImage.DOFade(0, tweenDuration / 2);
        baloon.MoveOffScreen();
        baloon.FadeOutGreyBaloons();
    }

    public void Notify_InfoScreenClosed()
    {
        if(score == baloonToInfoScreenDict.Count)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        masterScreenManager.ShowScreen(endHubScreen);
    }

    #endregion

}

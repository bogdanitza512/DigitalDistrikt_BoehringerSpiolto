﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class UtilityScript_ScrollFiller : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameObject template;

    public string nameTemplate;

    public List<Sprite> sprites;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void FlipList()
    {
        sprites.Reverse();
    }

    [Button(ButtonSizes.Medium)]
	public void FillScroll()
    {
        foreach(Sprite sprite in sprites)
        {
            var go = Instantiate(template, transform);
            var index = sprites.IndexOf(sprite);
            go.GetComponent<Image>().sprite = sprite;
            go.name = nameTemplate
                .FormatWith(index.ToString("00"));

        }
    }

    #endregion

}
